import json

from os.path import join

from flask import Flask, request


app = Flask(__name__)

traits = []
with open(join("data", "traits.json"), "r") as f:
    traits = json.load(f)


@app.route("/")
def index():
    return {"status": "ok"}


@app.route("/traits/<trait_id>", methods=["GET"])
def filter_traits(trait_id):
    for t in traits:
        if t.get("id") == trait_id:
            return t

    return {}


@app.route("/traits/types", methods=["POST"])
def filter_traits_types():
    results = []
    payload = request.get_json()
    trait = payload.get("trait", "")
    name = payload.get("name", "")
    if not trait:
        return results

    for t in traits:
        equal_trait = t.get("trait") == trait
        equal_name = t.get("name") == name
        if not name and equal_trait:
            results.append(t)
        elif equal_trait and equal_name:
            results.append(t)

    return results


@app.route("/traits/projects", methods=["POST"])
def filter_traits_projects():
    results = []
    payload = request.get_json()
    project = payload.get("project", "")
    if not project:
        return results

    for t in traits:
        equal_project1 = t.get("project1", {}).get("name", "") == project
        equal_project2 = t.get("project2", {}).get("name", "") == project
        if equal_project1 or equal_project2:
            results.append(t)

    return results
