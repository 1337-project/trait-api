import copy
import json

import pandas as pd


from pprint import pprint


TRAITS_SHEET = open("data/traits_sample.xlsx", "rb")


df_traits = pd.read_excel(
    TRAITS_SHEET,
    sheet_name="Traits",
    header=0,
    dtype={
        "id": str,
        "trait": str,
        "name": str,
        "project1": str,
        "project2": str,
        532: str,
    },
)
df_532 = pd.read_excel(
    TRAITS_SHEET,
    sheet_name="532",
    header=0,
    dtype={
        "id": str,
        "name": str,
        "twitter": str,
    },
)
df_projects = pd.read_excel(
    TRAITS_SHEET,
    sheet_name="Projects",
    header=0,
    dtype={
        "id": str,
        "type": str,
        "name": str,
        "contract": str,
        "website": str,
        "twitter": str,
        "opensea": str,
    },
)

results = []
df_532 = df_532.set_index("id").to_dict(orient="index")
df_projects = df_projects.set_index("id").to_dict(orient="index")
for _, row in df_traits.iterrows():
    d = {
        "id": row["id"],
        "trait": row["trait"],
        "name": row["name"],
    }

    for i in (1, 2):
        prefix = f"project{i}"
        pid = row[prefix]
        if isinstance(pid, float):
            pid = None

        d[f"{prefix}"] = {}
        d[f"{prefix}"]["id"] = pid
        for f in ["type", "name", "contract", "website", "twitter", "opensea"]:
            d[f"{prefix}"][f] = df_projects.get(pid, {}).get(f, None)

    ser = row[532]
    if isinstance(ser, float):
        ser = None

    d["532"] = {}
    d["532"]["id"] = ser
    for f in ["name", "twitter"]:
        d["532"][f] = df_532.get(ser, {}).get(f, None)

    pprint(d)
    results.append(d)


payload = json.dumps(results, indent=4)
with open("data/traits.json", "w") as f:
    f.write(json.dumps(results, indent=4))
