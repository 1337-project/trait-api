# Trait API

## Developing

To run the API:
```
npm i -g vercel
vercel dev
```

Your application is now available at `http://localhost:3000`.

To normalize the data,
```
poetry run python normalize.py
```

This will aggregate and normalize the data in the original Excel file inside `data` directory and overwrite/create a new one call `traits.json` in the same directory. This JSON file serves as the data source for the API.
